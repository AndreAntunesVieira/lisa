import icons from "./icons";
import explore from "./explore";

export default {
  icons,
  explore,
  logo: require("./logo/logo.png"),
  logoLarge: require("./logoLarge/logoLarge.png"),
  imageBackgroundMenuLeft: require("./imageBackgroundMenu/imageBackgroundMenuLeft.png"),
  imageBackgroundMenuRight: require("./imageBackgroundMenu/imageBackgroundMenuRight.png"),
  backgroundBlur: require("./backgroundBlur/backgroundBlur.png"),
  userPlaceholder: require("./user-placeholder.png"),
  bgPins: require("./bgpins.png")
};
