import "redux"
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux' // 5.0.6
import { HighscoreApi } from '../helpers/DatabaseHelpers'

import { resetToPlayAgain } from './../state/modules/AllActions'
import { navigateWithoutBack } from './../navigation'

import { 
  Button,
  ResultHeadline,
  ResultReport,
} from './../components'

import { FallbackUI, withErrorBoundary } from './../error'

class ResultScreen extends Component {
  componentWillMount(){
    HighscoreApi.push({score: this.props.score, name: this.props.name })
  }

  handlePlayAgain = () => {
    const { navigation, resetToPlayAgain } = this.props
    resetToPlayAgain()
    navigation.dispatch(navigateWithoutBack('Name'))
  }

  render() {
    const { results, score, numberOfQuestions, isLoading } = this.props

    if (!results || results.length === 0 && !isLoading) {
      const errorMessage = 'Unable to show results'
      throw new Error(errorMessage)
    }
  
    return (
      <View style={styles.container}> 
        <ResultHeadline score={score} numberOfQuestions={numberOfQuestions} />
        <ResultReport results={results} />
        
        <Button label='PLAY AGAIN' onPress={this.handlePlayAgain} />
      </View>
    )
  }
}


const mapStateToProps = ({ answer: { score, results }, fetchQuestion: { numberOfQuestions }, status: { isLoading }, user: { name } }) => {
  return { results, score, numberOfQuestions, isLoading, name }
}

const connectedResultScreen = connect(mapStateToProps, { resetToPlayAgain })(ResultScreen)

export default withErrorBoundary(connectedResultScreen, FallbackUI)

const styles = StyleSheet.create(
  {
    container: {
      flex: 1,
      justifyContent: 'space-around',
      backgroundColor: '#A2E4F9',
      width: '100%',
      alignItems: 'center',
      padding: 10,
    },
  }
)
