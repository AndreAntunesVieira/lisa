import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Text, View, StyleSheet, TextInput} from 'react-native'
import { Button } from './../components'
import { navigateWithoutBack } from './../navigation'

class NameScreen extends Component{
  render(){
    const { navigation, dispatch } = this.props
    const { containerStyle, headerStyle, descriptionStyle, inputStyle } = styles
    return (
      <View style={containerStyle}>
        <Text style={headerStyle}>Before you start please enter your 3 initials</Text>
        <Text style={descriptionStyle}>(initial of first name, initial of middle name, initial of last name).Thank you!</Text>
        <TextInput style={inputStyle} onChangeText={newValue => {
          dispatch(
            { type: 'SET_USER_NAME', name: newValue }
          )
        }} />
        <Button label='NEXT' onPress={() => navigation.dispatch(navigateWithoutBack('Home'))}/>
      </View>
    )
  }
}

export default connect()(NameScreen)

const styles = StyleSheet.create(
  {
    containerStyle: {
      flex: 1,
      justifyContent: 'space-around',
      backgroundColor: '#A2E4F9',
      width: '100%',
      alignItems: 'center',
      padding: 20,
    },
    headerStyle: {
      color: 'black',
      fontSize: 40,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    descriptionStyle: {
      color: 'black',
      fontSize: 30,
      textAlign: 'center'
    },
    callToActionStyle: {
      color: 'black',
      fontSize:30,
    },
    inputStyle: {
      backgroundColor: 'red',
      width: 100,
      height: 40,
      textAlign: 'center'
    }
  }
)
