import React from 'react'
import { Text, View, TouchableWithoutFeedback } from 'react-native'



const Button = (props) => {
    const { textStyle, viewStyle } = styles
    
    return (
      <TouchableWithoutFeedback onPress={props.onPress}>
        <View style={viewStyle}>
         <Text style={textStyle}>{props.label}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
}

const styles = {
  viewStyle: {
    padding: 5,
    backgroundColor: '#6BC1E9',
    borderRadius: 10,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'center',
},

  textStyle: {
    fontSize: 25,
    color: 'white',
  },
}

export default Button