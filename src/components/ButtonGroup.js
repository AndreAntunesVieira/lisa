import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import Button from './Button';

export default class ButtonGroup extends Component {

  renderList = () => {
    let array = Object.keys(this.props.data);
    return array.map(key => {
      return (
        <Button 
          key={key}
          label={this.props.data[key].title}
          //isChecked={this.props.data[key].value}
          onPress={() => this.props.onPress(key)}
        />
      )
    })
  }
  
  render() {
    return (
      <View style={styles.container}>
        {this.renderList()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  }
});