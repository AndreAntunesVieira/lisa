import React from 'react'
import { View, StyleSheet } from 'react-native'
import Button from './Button'
//import ButtonGroup from './ButtonGroup'

const renderButtons = (randomlySortedOptions, onPress) => {
  return (
   randomlySortedOptions.map(option => <Button key={option} label={option} onPress={() => onPress(option)}/> )
  
  )
}

const Answer = ({ onPress, correctAnswer, incorrectAnswers }) => {

    const { containerStyle } = styles
    const randomlySortedOptions = [correctAnswer, ...incorrectAnswers]
  
    
    return (
      <View style={containerStyle}>
      {
        renderButtons(randomlySortedOptions, onPress)
      }
       </View>
    );
}


const styles = StyleSheet.create(
  {
    containerStyle: {
      width: '80%',
      justifyContent: 'space-around'
    },
  }
)

export default Answer


