const INITIAL_STATE = {}

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'SET_USER_NAME': {
      return {
        ...state,
        name: action.name
      }
    }

    default:
      return state
  }
}
