import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyC6eSujShM7AGJ7YYHnE7UiVRuvRFPv154",
  authDomain: "lisamultiplechoises.firebaseapp.com",
  databaseURL: "https://lisamultiplechoises.firebaseio.com",
  projectId: "lisamultiplechoises",
  storageBucket: "lisamultiplechoises.appspot.com",
  messagingSenderId: "132150467200"
}
firebase.initializeApp(config)

export default firebase.database().ref('highscore')
