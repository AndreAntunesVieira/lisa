import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import firebase from 'firebase'

export default class App extends React.Component {
  componentWillMount(){
    const config = {
      apiKey: "AIzaSyC6eSujShM7AGJ7YYHnE7UiVRuvRFPv154",
      authDomain: "lisamultiplechoises.firebaseapp.com",
      databaseURL: "https://lisamultiplechoises.firebaseio.com",
      projectId: "lisamultiplechoises",
      storageBucket: "lisamultiplechoises.appspot.com",
      messagingSenderId: "132150467200"
    }
    firebase.initializeApp(config)

    firebase.database().ref('highscore').once('value')
      .then(response => {
        response.forEach(doc => {
          console.log(doc.toJSON())
        })
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>HighScore</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})
