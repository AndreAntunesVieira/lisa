import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import HighScoreApi from './HighScoreApi'

export default class App extends React.Component {
  componentWillMount(){
    HighScoreApi.once('value')
      .then(response => {
        response.forEach(doc => {
          console.log(doc.toJSON())
        })
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>HighScore</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})
