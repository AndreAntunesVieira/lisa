import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import HighScoreApi from './HighScoreApi'

export default class App extends React.Component {
  state = { highscore: [] }

  componentWillMount(){
    const highscore = []
    HighScoreApi.orderByChild('score').once('value')
      .then(response => {
        response.forEach(doc => {
          highscore.push(doc.toJSON())
        })
        this.setState({ highscore: highscore })
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>HighScore</Text>

        {this.state.highscore.map((doc, key) => (
          <Text key={key} style={styles.row}>
            <Text style={styles.text1}>
              Score: <Text style={styles.bold}>{ doc.score }</Text>,
            </Text>
            <Text style={styles.text2}>
              Name: <Text style={styles.bold}>{ doc.user }</Text>
            </Text>
          </Text>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bold: {
    fontWeight: 'bold'
  },
  row:{
  }
});
