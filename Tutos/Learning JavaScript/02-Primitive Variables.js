/*
Primitive variables
*/

/*
--- Empty variables --- 
*/
var some_variable;
var is_undefined = undefined;
var is_null = null;

/*
 -- Booleans --
*/

var is_true = true;
var is_false = false;

/*
 --- Number ---
 Different of other languages, JavaScript have no a big difference between Integers, Floats, Doubles...
*/

var intNumber = 10;
var floatNumber = 5.2;
var longNumber = 4e10; // = 40000000000

/* Numbers operations */
var number1 = 2 + 2;
var number2 = 5 - 3;
var number3 = 2 * 5;
var number4 = 10 / 3; // 3.3333333333333335 some other languages answer is just 3
var number5 = 10 % 3; // Just like other languages the answer is 1


/*
 --- Strings ---
 Recently JavaScript allow to write strings in 3 ways
 - Using single quotes ( ' )
 - Using double quotes ( " )
 - Using template literal quotes ( ` )
 */

var string1 = 'Some string';
var string2 = "Other string";
var string3 = `One more string`;




// Number functions
var is_not_a_number = Number.isNaN('Some String');
var is_a_number = Number.isNaN(130.1820);

var is_integer = Number.isInteger(10);
var is_not_integer = Number.isInteger(25.12390);

// Number prototypes
var number_as_string = intNumber.toString(); // var number_as_string = '10'
var number_as_string_fixed2 = intNumber.toFixed(2); // var number_as_string = '10.00'


// Strings Concatenations
var string4 = 'Some string' + "and other string"; // 'Some stringand other string'
var string5 = string1 + ' and ' + string2;      // 'Some string and Other string'
var string6 = `${string1} and ${string2}`;      // 'Some string and Other string'
var string7 = string1.concat(' and ', string2); // 'Some string and Other string'

// String prototypes


