const config = {
  amount: 11,
  type: 'multiple',
}
const baseUri = 'https://opentdb.com/api.php'
const defaultErrorMessage = 'Sorry, something went wrong.'
export async function getQuestions() {
  const { amount, difficulty, type } = config
  const from = `${baseUri}?amount=${amount}&type=${type}`
  return fetch(from)
    .then(response => response.json())
    .then(responseJson => ({ status: 'success', data: responseJson.results }))
    .catch(() => ({ status: 'fail', errorMessage: defaultErrorMessage }))
};
