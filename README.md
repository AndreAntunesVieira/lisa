# Lisa Multiple Choices App

## Intro

This is a sample project with the objective of learn React Native

## Installation

Download the project using `git` structure.


Run on terminal the command below:
```bash
git clone https://gitlab.com/AndreAntunesVieira/lisa.git
```

This command will download all project files from [GitLab](https://gitlab.com) server.

Now you need to go to the project folder:
```bash
cd lisa
```

And install the external dependencies:
```bash
npm install
```

This command will install all code developed from other programmers and companies like `react`, `react-native`, `redux`, `expo`... and save all on inside the folder `node_modules`.

## Running


You have 3 ways to run this app, from your phone (using [Expo APP]()), using a iPhone simulator (This command only works on MacOS) and using Android Simulator.

### Running on your smart phone\

You need to install [Expo App]() on your phone and on your computer you must run on terminal the command:
```bash
npm start
```

This command will show on terminal a QR code, on your phone you must go to Expo app and ask for scan a new QR code.
This steps will run the app on your phone.

### iOS simulator

iOS simulator only works on MacOS (Macbook, mac mini, Mac Pro...). You must be with your `XCode` updated. You just need to run the command below on your terminal.
```bash
npm run ios
```
This command will show the same QR code on your terminal (so you can run your phone the app on the same time) and open a iPhone simulator using the
XCode simulator and then will run the Expo app with your project inside.


### Android simulator
You need to install the Android Studio to can use the simulator. Then you just need to run the command below on your terminal.

```bash
npm run android
```
